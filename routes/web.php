<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/faculty.blade.php', function () {
    return view('faculty');
});
Route::get('/home.blade.php', function () {
    return view('home');
});
Route::get('/Abu Hasnat Mohammad Ashfak Habib.blade.php', function () {
    return view('Abu Hasnat Mohammad Ashfak Habib');
});
Route::get('/admission.blade.php', function () {
    return view('admission');
});
Route::get('/Animesh Chandra Roy.blade.php', function () {
    return view('Animesh Chandra Roy');
});
Route::get('/contact.blade.php', function () {
    return view('contact');
});
Route::get('/Asaduzzaman.blade.php', function () {
    return view('Asaduzzaman');
});
Route::get('/Kaushik Deb.blade.php', function () {
    return view('Kaushik Deb');
});
Route::get('/Farzana Yasmin.blade.php', function () {
    return view('Farzana Yasmin');
});
Route::get('/Ibrahim Khan.blade.php', function () {
    return view('Ibrahim Khan');
});
Route::get('/Jibon Naher.blade.php', function () {
    return view('Jibon Naher');
});
Route::get('/samsul arefin.blade.php', function () {
    return view('samsul arefin');
});
Route::get('/Lamia Alam.blade.php', function () {
    return view('Lamia Alam');
});
Route::get('/Mahfuzulhoq Chowdhury.blade.php', function () {
    return view('Mahfuzulhoq Chowdhury');
});
Route::get('/master.blade.php', function () {
    return view('master');
});
Route::get('/Enamul Hoque Prince.blade.php', function () {
    return view('Enamul Hoque Prince');
});
Route::get('/Iqbal Hasan Sarker.blade.php', function () {
    return view('Iqbal Hasan Sarker');
});
Route::get('/Monjur-Ul-Hasan.blade.php', function () {
    return view('Monjur-Ul-Hasan');
});
Route::get('/Monjurul Islam.blade.php', function () {
    return view('Monjurul Islam');
});
Route::get('/Sabir Hossain.blade.php', function () {
    return view('Sabir Hossain');
});
Route::get('/Shafiul Alam Forhad.blade.php', function () {
    return view('Shafiul Alam Forhad');
});
Route::get('/Saki Kowsar.blade.php', function () {
    return view('Saki Kowsar');
});
